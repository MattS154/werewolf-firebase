import Vue from 'vue';
import Vuex from 'vuex';
import { vuexfireMutations } from 'vuexfire';
import modules from './modules';
// import types from '../constants/types';

// const {
//   CREATOR,
//   PLAYER,
//   VILLAGER,
//   WEREWOLF,
//   WEREWOLF_DECK,
//   THUMBS_UP,
//   THUMBS_DOWN,
// } = types;

Vue.use(Vuex);

const store = new Vuex.Store({
  modules,
  mutations: {
    ...vuexfireMutations,
  },
});

Object.keys(modules)
  .forEach((module) => {
    if (modules[module].actions && modules[module].actions.bind) {
      store.dispatch(`${module}/bind`);
    }
  });

// export default new Vuex.Store({
//   state: {
//     roomId: 'abc', // Vue router param
//     room: {
//       options: {
//         game: {
//           deck: WEREWOLF_DECK,
//           dayLength: (2 * 60 * 1000), // 2 minutes
//         },
//         voting: {
//           timer: (30 * 1000), // 30 seconds
//           evilThreshold: 1, // 100%
//           innocentThreshold: 0.5, // 50%
//         },
//       },
//       players: [
//         { // Player
//           id: 'someguid',
//           name: 'Matt',
//           type: CREATOR,
//           role: WEREWOLF,
//           thumb: THUMBS_UP,
//         },
//         { // Player
//           id: 'anotherguid',
//           name: 'Anna',
//           type: PLAYER,
//           role: VILLAGER,
//           thumb: THUMBS_DOWN,
//         },
//       ],
//     },
//   },
//   mutations: {
//     ...vuexfireMutations,
//   },
//   actions: {
//   },
//   modules: {
//   },
// });

export default store;

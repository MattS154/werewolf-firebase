import firebase from 'firebase/app';
import { firestoreAction } from 'vuexfire';

// eslint-disable-next-line no-unused-vars
import { firestore } from '@/firebase';

export default {
  state: {
    todos: [],
  },
  actions: {
    bind: firestoreAction(({ bindFirestoreRef }) => bindFirestoreRef('todos', firestore.collection('todos').orderBy('created', 'asc'))),
    unbind: firestoreAction(({ unbindFirestoreRef }) => {
      unbindFirestoreRef('todos');
    }),
    add: firestoreAction(({ state }, newTodo) => {
      console.log(newTodo, state);
      firestore.collection('todos')
        .add({
          // eslint-disable-next-line no-undef
          created: firebase.firestore.FieldValue.serverTimestamp(),
          ...newTodo,
        });
    }),
    // eslint-disable-next-line no-unused-vars
    update: firestoreAction(({ state }, todoRef) => {
      const todo = { ...todoRef };

      firestore.collection('todos')
        .doc(todoRef.id)
        .update(todo);
    }),
    // eslint-disable-next-line no-unused-vars
    delete: firestoreAction(({ state }, todoRef) => {
      firestore.collection('todos')
        .doc(todoRef.id)
        .delete();
    }),
  },
};

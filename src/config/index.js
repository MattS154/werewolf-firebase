import camelCase from 'lodash/camelCase';

const requireConfig = require.context('.', false, /\.js$/);
const modules = {};

requireConfig
  .keys()
  .forEach((fileName) => {
    if (fileName.indexOf('index.js') > -1) return;

    const moduleName = camelCase(
      fileName.replace(/(\.\/|\.js$)/g, ''),
    );

    const component = requireConfig(fileName);

    modules[moduleName] = component.default || component;
  });

export default modules;

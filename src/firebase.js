// Conveniently import this file anywhere to use db

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/analytics';
import config from '@/config';

firebase.initializeApp(config.firebase);

// eslint-disable-next-line import/prefer-default-export
export const firestore = firebase
  .app()
  .firestore();

// eslint-disable-next-line import/prefer-default-export
export const analytics = firebase
  .app()
  .analytics();

// Export types that exists in Firestore - Uncomment if you need them in your app
const { Timestamp, GeoPoint } = firebase.firestore;
export { Timestamp, GeoPoint };

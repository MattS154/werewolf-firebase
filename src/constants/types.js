const types = [
  // User types
  'CREATOR',
  'ADMIN',
  'HOST',
  'PLAYER',

  // Game roles
  'VILLAGER',
  'WEREWOLF',
  'SPELLCASTER',
  'TANNER',
  'WITCH',
  'VILLAGE_IDIOT',
  'LONE_WOLF',
  'DOPPELGANGER',
  'SEER',
  'BODYGUARD',

  // Deck types
  'WEREWOLF_DECK',

  // Voting
  'THUMBS_UP',
  'THUMBS_DOWN',
];

// Create object so its like { WEREWOLF: 'WEREWOLF' }
export default types.reduce((obj, value) => {
  // eslint-disable-next-line no-param-reassign
  obj[value] = value;
  return obj;
}, {});
